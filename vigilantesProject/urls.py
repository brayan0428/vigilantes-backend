from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from vigilantesApp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('vigilante-create/', views.create, name="vigilante-create"),
    path('vigilante/', views.get_all, name="vigilante-get-all"),
    path('vigilante-create/', views.create, name="vigilante-create"),
    path('vigilante-update/<str:pk>', views.update, name="vigilante-update"),
    path('vigilante-delete/<str:pk>', views.delete, name="vigilante-delete"),
    path('cargos/', views.get_cargos, name="cargos"),
    path('clientes/', views.get_clientes, name="clientes"),
    path('ciudades/', views.get_ciudades, name="ciudades"),
    path('escalas/', views.get_escalas, name="escalas"),
    path('salarios/', views.get_salarios, name="salarios")
]
