from django.apps import AppConfig


class VigilantesappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vigilantesApp'
