from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from vigilantesApp.serializers.vigilanteSerializer import VigilanteSerializer
from vigilantesApp.models.vigilante import Vigilante


@api_view(['GET'])
def get_all(request):
    vigilantes = Vigilante.objects.all()
    serializer = VigilanteSerializer(vigilantes, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def create(request):
    try:
        serializer = VigilanteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except:
        return Response({"error": "Error al momento de procesar la solicitud"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['PUT'])
def update(request, pk):
    try:
        vigilante = Vigilante.objects.get(id=pk)
        serializer = VigilanteSerializer(instance=vigilante, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except:
        return Response({"error": "Error al momento de procesar la solicitud"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['DELETE'])
def delete(request, pk):
    try:
        vigilante = Vigilante.objects.get(id=pk)
        vigilante.delete()
        return Response(status=status.HTTP_200_OK)
    except:
        return Response({"error": "Error al momento de procesar la solicitud"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
