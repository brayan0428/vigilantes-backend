from rest_framework.decorators import api_view
from rest_framework.response import Response
from vigilantesApp.serializers.otherSerializer import CargoSerializer, ClienteSerializer, CiudadSerializer, SalarioSerializer, EscalaSerializer
from vigilantesApp.models.cargo import Cargo
from vigilantesApp.models.cliente import Cliente
from vigilantesApp.models.ciudad import Ciudad
from vigilantesApp.models.salario import Salario
from vigilantesApp.models.escala import Escala


@api_view(['GET'])
def get_cargos(request):
    cargos = Cargo.objects.all()
    serializer = CargoSerializer(cargos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_clientes(request):
    clientes = Cliente.objects.all()
    serializer = ClienteSerializer(clientes, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_ciudades(request):
    ciudades = Ciudad.objects.all()
    serializer = CiudadSerializer(ciudades, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_escalas(request):
    escalas = Escala.objects.all()
    serializer = EscalaSerializer(escalas, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_salarios(request):
    salarios = Salario.objects.all()
    serializer = SalarioSerializer(salarios, many=True)
    return Response(serializer.data)
