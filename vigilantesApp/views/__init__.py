from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .vigilanteView import create, get_all, update, delete
from .otherView import get_cargos, get_clientes, get_ciudades, get_escalas, get_salarios
