from vigilantesApp.models.cargo import Cargo
from vigilantesApp.models.cliente import Cliente
from vigilantesApp.models.ciudad import Ciudad
from vigilantesApp.models.salario import Salario
from vigilantesApp.models.escala import Escala
from rest_framework import serializers


class CargoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cargo
        fields = ['id', 'nombre', 'activo']


class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ['id', 'nombre', 'activo']


class SalarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Salario
        fields = ['id', 'nombre', 'activo']


class EscalaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Escala
        fields = ['id', 'nombre', 'activo']


class CiudadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ciudad
        fields = ['id', 'nombre', 'activo']
