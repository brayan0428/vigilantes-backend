from vigilantesApp.models.vigilante import Vigilante
from rest_framework import serializers


class VigilanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vigilante
        fields = ['id', 'cedula', 'nombre', 'apellido1', 'apellido2', 'salario', 'fecha_ingreso',
                  'email', 'direccion', 'ciudad', 'telefono', 'cargo', 'escala', 'cliente']
