from .userSerializer import UserSerializer
from .vigilanteSerializer import VigilanteSerializer
from .otherSerializer import CargoSerializer, ClienteSerializer, CiudadSerializer, EscalaSerializer, SalarioSerializer
