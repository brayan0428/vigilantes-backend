from django.db import models


class Cargo(models.Model):
    nombre = models.CharField(max_length=60)
    activo = models.BooleanField(default=True)
