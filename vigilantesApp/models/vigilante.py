from django.db import models
from datetime import date
from .cargo import Cargo
from .salario import Salario
from .escala import Escala
from .cliente import Cliente
from .ciudad import Ciudad


class Vigilante(models.Model):
    cedula = models.IntegerField()
    nombre = models.CharField(max_length=60)
    apellido1 = models.CharField(max_length=30)
    apellido2 = models.CharField(max_length=30)
    salario = models.ForeignKey(
        Salario, related_name='salario', on_delete=models.CASCADE)
    fecha_ingreso = models.DateField(default=date.today)
    email = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    ciudad = models.ForeignKey(
        Ciudad, related_name='ciudad', on_delete=models.CASCADE)
    telefono = models.IntegerField(null=True)
    cargo = models.ForeignKey(
        Cargo, related_name='cargo', on_delete=models.CASCADE)
    escala = models.ForeignKey(
        Escala, related_name='escala', on_delete=models.CASCADE)
    cliente = models.ForeignKey(
        Cliente, related_name='cargo', on_delete=models.CASCADE)

    def __str__(self):
        return "{self.nombre} {self.apellido1} {self.apellido2}"
